# i want to install k8s cluster on RHEL-7 VM - in offline mode
# VM
RHEL-7
# #################  RPM INSTALLATION
# YUM
    - yum/
yum-utils-1.1.31-54.el7_8.noarch.rpm:  
# DM
    - dm/
device-mapper-persistent-data-0.8.5-3.el7_9.2.x86_64.rpm:  
libaio-0.3.109-13.el7.x86_64.rpm:  
# LVM2
    - lvm2/
device-mapper-1.02.170-6.el7_9.5.x86_64.rpm:  
device-mapper-event-1.02.170-6.el7_9.5.x86_64.rpm:  
device-mapper-event-libs-1.02.170-6.el7_9.5.x86_64.rpm:  
device-mapper-libs-1.02.170-6.el7_9.5.x86_64.rpm:  
device-mapper-persistent-data-0.8.5-3.el7_9.2.x86_64.rpm:  
libaio-0.3.109-13.el7.x86_64.rpm:  
lvm2-2.02.187-6.el7_9.5.x86_64.rpm:  
lvm2-libs-2.02.187-6.el7_9.5.x86_64.rpm:  
# SE
    - se/
audit-libs-python-2.8.5-4.el7.x86_64.rpm:  
checkpolicy-2.5-8.el7.x86_64.rpm:  
container-selinux-2.119.2-1.911c772.el7_8.noarch.rpm:  
libcgroup-0.41-21.el7.x86_64.rpm:  
libselinux-python-2.5-15.el7.x86_64.rpm:  
libselinux-utils-2.5-15.el7.x86_64.rpm:  
libsemanage-python-2.5-14.el7.x86_64.rpm:  
policycoreutils-2.5-34.el7.x86_64.rpm:  
policycoreutils-python-2.5-34.el7.x86_64.rpm:  
python-IPy-0.75-6.el7.noarch.rpm:  
selinux-policy-3.13.1-268.el7_9.2.noarch.rpm:  
selinux-policy-targeted-3.13.1-268.el7_9.2.noarch.rpm:  
setools-libs-3.3.8-4.el7.x86_64.rpm:  
# DOCKER
    - docker-ce/
audit-libs-python: Version 2.8.5-4.el7.rpm:    
checkpolicy: Version 2.5-8.el7.rpm:    
container-selinux: Version 2.119.2-1.911c772.el7_8.rpm:    
containerd.io: Version 1.6.32-3.1.el7.rpm:    
docker-buildx-plugin: Version 0.14.0-1.el7.rpm:    
docker-ce: Version 26.1.3-1.el7.rpm:    
docker-ce-cli: Version 26.1.3-1.el7.rpm:    
docker-ce-rootless-extras: Version 26.1.3-1.el7.rpm:    
docker-compose-plugin: Version 2.27.0-1.el7.rpm:    
fuse-overlayfs: Version 0.7.2-6.el7_8.rpm:    
fuse3-libs: Version 3.6.1-4.el7.rpm:    
iptables: Version 1.4.21-35.el7.rpm:    
libcgroup: Version 0.41-21.el7.rpm:    
libmnl: Version 1.0.3-7.el7.rpm:    
libnetfilter_conntrack: Version 1.0.6-1.el7_3.rpm:    
libnfnetlink: Version 1.0.1-4.el7.rpm:    
libseccomp: Version 2.3.1-4.el7.rpm:    
libselinux-python: Version 2.5-15.el7.rpm:    
libselinux-utils: Version 2.5-15.el7.rpm:    
libsemanage-python: Version 2.5-14.el7.rpm:    
policycoreutils: Version 2.5-34.el7.rpm:    
policycoreutils-python: Version 2.5-34.el7.rpm:    
python-IPy: Version 0.75-6.el7.rpm:    
selinux-policy: Version 3.13.1-268.el7_9.2.rpm:    
selinux-policy-targeted: Version 3.13.1-268.el7_9.2.rpm:    
setools-libs: Version 3.3.8-4.el7.rpm:    
slirp4netns: Version 0.4.3-4.el7_8.rpm:    
# KUBERNETES
    - kube/
conntrack-tools-1.4.4-7.el7.x86_64.rpm:  
libmnl-1.0.3-7.el7.x86_64.rpm:  
cri-tools-1.29.0-150500.1.1.x86_64.rpm:  
libnetfilter_conntrack-1.0.6-1.el7_3.x86_64.rpm:  
ebtables-2.0.10-16.el7.x86_64.rpm:  
ethtool-4.8-10.el7.x86_64.rpm:  
iproute-4.11.0-30.el7.x86_64.rpm:  
iptables-1.4.21-35.el7.x86_64.rpm:  
kubeadm-1.29.5-150500.1.1.x86_64.rpm:  
kubectl-1.29.5-150500.1.1.x86_64.rpm:  
kubelet-1.29.5-150500.1.1.x86_64.rpm:  
kubernetes-cni-1.3.0-150500.1.1.x86_64.rpm:  
libnetfilter_cthelper-1.0.0-11.el7.x86_64.rpm:  
libnetfilter_cttimeout-1.0.0-7.el7.x86_64.rpm:  
libnetfilter_queue-1.0.2-2.el7_2.x86_64.rpm:  
libnfnetlink-1.0.1-4.el7.x86_64.rpm:  
socat-1.7.3.2-2.el7.x86_64.rpm:  
tcp_wrappers-libs-7.6-77.el7.x86_64.rpm:  
yum-utils-1.1.31-54.el7_8.noarch.rpm:  
# #################  KUBERNETES-IMAGES
    - docker-images/
coredns_v1.11.1.tar:
kube-apiserver_v1.29.5.tar:
kube-proxy_v1.29.5.tar:
pause_3.9.tar:
etcd_v3.5.12-0.tar:
kube-controller-manager_v1.29.5.tar:
kube-scheduler_v1.29.5.tar: 
# #################  NETWORKING
    - networking/
flannel-cni-p:  
flannel-cni-plugin-v1.4.1-flannel1.tar:  
flannel-v0.25.2.tar:  
flannel:v0.25.2.tar:  
kube-flannel.yml:  
# #################  NGINX
    - nginx/
deploy.yaml:  
kube-webhook-certgen_v1.5.0.tar:  
nginx-controller_v0.41.0.tar:  
# i want to install k8s cluster on RHEL-7 VM - in offline mode
# so before i install on offline vm, i try to install on  RHEL-7 docker container

# on my host:
swapoff -a 
docker run -it --name rhel7-container --privileged centos:7 /bin/bash
docker cp ~/kubernetes_last_chance/ rhel7-container:/var/rpm_dir/
docker exec -it rhel7-container /bin/bash
# inside the container
chmod +x /var/rpm_dir/k8s_installation_script.sh
./var/rpm_dir/k8s_installation_script.sh | tee k8s_installation_log.txt
                #!/bin/bash
                set -e  # Exit immediately if a command exits with a non-zero status
                set -x  # Print each command before executing
                echo "Disabling swap..."
                swapoff -a
                echo "Installing RPM packages..."
                yum install -y --cacheonly --disablerepo=* /var/rpm_dir/dm/*.rpm
                yum install -y --cacheonly --disablerepo=* /var/rpm_dir/lvm2/*.rpm
                yum install -y --cacheonly --disablerepo=* /var/rpm_dir/se/*.rpm
                yum install -y --cacheonly --disablerepo=* /var/rpm_dir/docker-ce/*.rpm
                yum install -y --cacheonly --disablerepo=* /var/rpm_dir/kube/*.rpm
                echo "Starting Docker..."
                dockerd &
                sleep 10
                if ! pgrep -x "dockerd" > /dev/null; then
                echo "Docker failed to start." >&2
                exit 1
                fi
                echo "Docker started successfully."
                echo "Loading Docker images..."
                cd /var/rpm_dir/docker-images/
                for image_tar in *.tar; do
                if [[ -f "$image_tar" ]]; then
                        docker load < "$image_tar"
                        echo "Loaded Docker image from $image_tar"
                else
                        echo "Docker image tar file $image_tar does not exist."
                fi
                done
                echo "Docker images loaded successfully."
                echo "Copying and extracting CNI plugins..."
                mkdir -p /etc/cni/net.d
                cp /var/rpm_dir/networking/flannel-cni-plugin-v1.4.1-flannel1.tar /etc/cni/net.d/
                cd /etc/cni/net.d
                for plugin_tar in *.tar; do
                if [[ -f "$plugin_tar" ]]; then
                        tar -xf "$plugin_tar"
                        echo "Extracted CNI plugin from $plugin_tar"
                else
                        echo "CNI plugin tar file $plugin_tar does not exist."
                fi
                done
                echo "CNI plugins extracted successfully."
                echo "Creating CNI configuration file..."
                cat <<EOF > /etc/cni/net.d/10-flannel.conflist
                {
                "name": "cni-flannel",
                "cniVersion": "0.3.1",
                "plugins": [
                {
                "type": "flannel",
                "delegate": {
                        "isDefaultGateway": true
                }
                }
                ]
                }
                EOF
                echo "CNI configuration file created successfully."
                mkdir -p /var/run/docker/containerd/
                mkdir -p /etc/containerd/
                cp /var/rpm_dir/config.toml /etc/containerd/
                cp /var/rpm_dir/containerd.toml /var/run/docker/containerd/
                echo "Starting containerd..."
                containerd -c /etc/containerd/config.toml &
                sleep 10
                if ! pgrep -x "containerd" > /dev/null; then
                echo "containerd failed to start." >&2
                exit 1
                fi
                echo "containerd started successfully."
                echo "Generating kubeadm config..."
                cat <<EOF > /etc/kubernetes/kubeadm-config.yaml
                apiVersion: kubeadm.k8s.io/v1beta3
                kind: ClusterConfiguration
                imageRepository: "registry.k8s.io"
                kubernetesVersion: "v1.29.5"
                EOF
                echo "kubeadm config generated."
                echo "Starting kubelet..."
                kubelet &
                sleep 10
                if ! pgrep -x "kubelet" > /dev/null; then
                echo "kubelet failed to start." >&2
                exit 1
                fi
                echo "kubelet started successfully."
                echo "Initializing Kubernetes cluster..."
                kubeadm init --config /etc/kubernetes/kubeadm-config.yaml --ignore-preflight-errors=CRI,Service-Kubelet
                mkdir -p $HOME/.kube
                cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
                chown $(id -u):$(id -g) $HOME/.kube/config
                echo "Checking Kubernetes component statuses..."
                kubectl get nodes
                kubectl get pods --all-namespaces
                echo "Installation and configuration completed successfully."
# in this step i have output: 
from dockerd:
                        INFO[2024-06-02T09:01:02.633777122Z] containerd successfully booted in 0.010447s
                        INFO[2024-06-02T09:01:03.625565494Z] [graphdriver] using prior storage driver: fuse-overlayfs
                        INFO[2024-06-02T09:01:03.628235094Z] Loading containers: start.
                        INFO[2024-06-02T09:01:03.671540504Z] Default bridge (docker0) is assigned with an IP address 172.18.0.0/16. Daemon option --bip can be used to set a preferred IP address
                        INFO[2024-06-02T09:01:03.690638308Z] Loading containers: done.
                        WARN[2024-06-02T09:01:03.693974315Z] WARNING: No blkio throttle.read_bps_device support
                        WARN[2024-06-02T09:01:03.693993024Z] WARNING: No blkio throttle.write_bps_device support
                        WARN[2024-06-02T09:01:03.693995772Z] WARNING: No blkio throttle.read_iops_device support
                        WARN[2024-06-02T09:01:03.693997834Z] WARNING: No blkio throttle.write_iops_device support
                        INFO[2024-06-02T09:01:03.694017925Z] Docker daemon                                 commit=8e96db1 containerd-snapshotter=false storage-driver=fuse-overlayfs version=26.1.3
                        INFO[2024-06-02T09:01:03.694052470Z] Daemon has completed initialization
                        INFO[2024-06-02T09:01:03.727317131Z] API listen on /var/run/docker.sock
from containerd:
                        erd.grpc.v1.cri StateDir:/run/containerd/io.containerd.grpc.v1.cri}
                        INFO[2024-06-02T09:01:32.208625084Z] Connect containerd service
                        INFO[2024-06-02T09:01:32.208685166Z] Get image filesystem path "/var/lib/containerd/io.containerd.snapshotter.v1.overlayfs"
                        INFO[2024-06-02T09:01:32.209118420Z] Start subscribing containerd event
                        INFO[2024-06-02T09:01:32.209185540Z] Start recovering state
                        INFO[2024-06-02T09:01:32.209237961Z] Start event monitor
                        INFO[2024-06-02T09:01:32.209251203Z] serving...                                    address=/run/containerd/containerd.sock.ttrpc
                        INFO[2024-06-02T09:01:32.209266686Z] Start snapshots syncer
                        INFO[2024-06-02T09:01:32.209339900Z] Start cni network conf syncer for default
                        INFO[2024-06-02T09:01:32.209346051Z] Start streaming server
                        INFO[2024-06-02T09:01:32.209348643Z] serving...                                    address=/run/containerd/containerd.sock
                        INFO[2024-06-02T09:01:32.209400528Z] containerd successfully booted in 0.011501s
from kubelet:
                        I0602 09:01:44.168938     913 server.go:1256] "Started kubelet"
                        I0602 09:01:44.168976     913 kubelet.go:1610] "No API server defined - no node status update will be sent"
                        I0602 09:01:44.168987     913 server.go:162] "Starting to listen" address="0.0.0.0" port=10250
                        I0602 09:01:44.169074     913 ratelimit.go:55] "Setting rate limiting for endpoint" service="podresources" qps=100 burstTokens=10
                        I0602 09:01:44.169145     913 server.go:194] "Starting to listen read-only" address="0.0.0.0" port=10255
                        I0602 09:01:44.169434     913 server.go:233] "Starting to serve the podresources API" endpoint="unix:/var/lib/kubelet/pod-resources/kubelet.sock"
                        I0602 09:01:44.169734     913 fs_resource_analyzer.go:67] "Starting FS ResourceAnalyzer"
                        I0602 09:01:44.170539     913 volume_manager.go:291] "Starting Kubelet Volume Manager"
                        I0602 09:01:44.170628     913 desired_state_of_world_populator.go:151] "Desired state populator starts to run"
                        I0602 09:01:44.170684     913 reconciler_new.go:29] "Reconciler: start to sync state"
                        I0602 09:01:44.170969     913 server.go:461] "Adding debug handlers to kubelet server"
                        E0602 09:01:44.171082     913 kubelet.go:1462] "Image garbage collection failed once. Stats initialization may not have completed yet" err="invalid capacity 0 on image filesystem"
                        I0602 09:01:44.171690     913 factory.go:219] Registration of the crio container factory failed: Get "http://%2Fvar%2Frun%2Fcrio%2Fcrio.sock/info": dial unix /var/run/crio/crio.sock: connect: no such file or directory
                        I0602 09:01:44.173158     913 factory.go:221] Registration of the containerd container factory successfully
                        I0602 09:01:44.173181     913 factory.go:221] Registration of the systemd container factory successfully
                        I0602 09:01:44.176407     913 kubelet_network_linux.go:50] "Initialized iptables rules." protocol="IPv4"
                        I0602 09:01:44.177046     913 kubelet_network_linux.go:50] "Initialized iptables rules." protocol="IPv6"
                        I0602 09:01:44.177092     913 status_manager.go:213] "Kubernetes client is nil, not starting status manager"
                        I0602 09:01:44.177113     913 kubelet.go:2329] "Starting kubelet main sync loop"
                        E0602 09:01:44.177150     913 kubelet.go:2353] "Skipping pod synchronization" err="[container runtime status check may not have completed yet, PLEG is not healthy: pleg has yet to be successful]"
                        I0602 09:01:44.177159     913 cpu_manager.go:214] "Starting CPU manager" policy="none"
                        I0602 09:01:44.177175     913 cpu_manager.go:215] "Reconciling" reconcilePeriod="10s"
                        I0602 09:01:44.177199     913 state_mem.go:36] "Initialized new in-memory state store"
                        I0602 09:01:44.177312     913 state_mem.go:88] "Updated default CPUSet" cpuSet=""
                        I0602 09:01:44.177366     913 state_mem.go:96] "Updated CPUSet assignments" assignments={}
                        I0602 09:01:44.177388     913 policy_none.go:49] "None policy: Start"
                        I0602 09:01:44.177757     913 memory_manager.go:170] "Starting memorymanager" policy="None"
                        I0602 09:01:44.177785     913 state_mem.go:35] "Initializing new in-memory state store"
                        I0602 09:01:44.177933     913 state_mem.go:75] "Updated machine memory state"
                        I0602 09:01:44.178594     913 manager.go:479] "Failed to read data from checkpoint" checkpoint="kubelet_internal_checkpoint" err="checkpoint is not found"
                        I0602 09:01:44.178826     913 plugin_manager.go:118] "Starting Kubelet Plugin Manager"
                        E0602 09:01:44.181913     913 summary_sys_containers.go:48] "Failed to get system container stats" err="failed to get cgroup stats for \"/docker/34a3718bf09b7bad4024c9abb27c372b65cb523d5c93f98ff19640d386a355b6\": failed to get container info for \"/docker/34a3718bf09b7bad4024c9abb27c372b65cb523d5c93f98ff19640d386a355b6\": unknown container \"/docker/34a3718bf09b7bad4024c9abb27c372b65cb523d5c93f98ff19640d386a355b6\"" containerName="/docker/34a3718bf09b7bad4024c9abb27c372b65cb523d5c93f98ff19640d386a355b6"
                        I0602 09:01:44.272130     913 desired_state_of_world_populator.go:159] "Finished populating initial desired state of world"

# now, can you help me to understand what i got so far, what is my issue, and how can i solve it. thanks!

####

# stop
docker stop rhel7-container
docker rm rhel7-container
