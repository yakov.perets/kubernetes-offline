# VM
RHEL-7
: verify: 
        - swapoff -a

# #################  RPM INSTALLATION
# YUM
    - yum/
yum-utils-1.1.31-54.el7_8.noarch.rpm:  
# Collection of utilities for managing YUM repositories and packages
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/yum/*.rpm

# DM
    - dm/
device-mapper-persistent-data-0.8.5-3.el7_9.2.x86_64.rpm:  
# Utilities for managing device mapper persistent metadata
libaio-0.3.109-13.el7.x86_64.rpm:  
# Library providing asynchronous I/O operations functionality
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/dm/*.rpm

# LVM2
    - lvm2/
device-mapper-1.02.170-6.el7_9.5.x86_64.rpm:  
# Device mapper libraries and tools
device-mapper-event-1.02.170-6.el7_9.5.x86_64.rpm:  
# Device mapper event daemon
device-mapper-event-libs-1.02.170-6.el7_9.5.x86_64.rpm:  
# Device mapper event library
device-mapper-libs-1.02.170-6.el7_9.5.x86_64.rpm:  
# Device mapper shared libraries
device-mapper-persistent-data-0.8.5-3.el7_9.2.x86_64.rpm:  
# Utilities for managing device mapper persistent metadata
libaio-0.3.109-13.el7.x86_64.rpm:  
# Library providing asynchronous I/O operations functionality
lvm2-2.02.187-6.el7_9.5.x86_64.rpm:  
# Logical Volume Manager 2
lvm2-libs-2.02.187-6.el7_9.5.x86_64.rpm:  
# Shared libraries for LVM2
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/lvm2/*.rpm

# SE
    - se/
audit-libs-python-2.8.5-4.el7.x86_64.rpm:  
# Python bindings for Linux Audit framework
checkpolicy-2.5-8.el7.x86_64.rpm:  
# Compiles SELinux security policies
container-selinux-2.119.2-1.911c772.el7_8.noarch.rpm:  
# SELinux policy for container environments
libcgroup-0.41-21.el7.x86_64.rpm:  
# Utilities for managing cgroups
libselinux-python-2.5-15.el7.x86_64.rpm:  
# Python bindings for SELinux
libselinux-utils-2.5-15.el7.x86_64.rpm:  
# Utilities for managing SELinux policies
libsemanage-python-2.5-14.el7.x86_64.rpm:  
# Python bindings for SELinux management library
policycoreutils-2.5-34.el7.x86_64.rpm:  
# Utilities for managing SELinux policies
policycoreutils-python-2.5-34.el7.x86_64.rpm:  
# Python bindings for SELinux policy management
python-IPy-0.75-6.el7.noarch.rpm:  
# Python library for handling IP addresses
selinux-policy-3.13.1-268.el7_9.2.noarch.rpm:  
# Core SELinux policy definitions and rules
selinux-policy-targeted-3.13.1-268.el7_9.2.noarch.rpm:  
# Targeted SELinux policy configuration
setools-libs-3.3.8-4.el7.x86_64.rpm:  
# Libraries for interacting with SELinux policy files
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/se/*.rpm


# DOCKER
    - docker-ce/
audit-libs-python: Version 2.8.5-4.el7.rpm:    
# Python bindings for Linux Audit framework
checkpolicy: Version 2.5-8.el7.rpm:    
# Compiles SELinux security policies
container-selinux: Version 2.119.2-1.911c772.el7_8.rpm:    
# SELinux policy for container environments
containerd.io: Version 1.6.32-3.1.el7.rpm:    
# Container runtime essential for managing containers
docker-buildx-plugin: Version 0.14.0-1.el7.rpm:    
# Extends Docker image building capabilities
docker-ce: Version 26.1.3-1.el7.rpm:    
# Docker Community Edition for containerization
docker-ce-cli: Version 26.1.3-1.el7.rpm:    
# Command-line interface for Docker
docker-ce-rootless-extras: Version 26.1.3-1.el7.rpm:    
# Additional components for running Docker rootless
docker-compose-plugin: Version 2.27.0-1.el7.rpm:    
# Extends Docker Compose functionality
fuse-overlayfs: Version 0.7.2-6.el7_8.rpm:    
# Lightweight union filesystem for containers
fuse3-libs: Version 3.6.1-4.el7.rpm:    
# Libraries for developing filesystems in userspace
iptables: Version 1.4.21-35.el7.rpm:    
# Linux firewall utility for packet filtering
libcgroup: Version 0.41-21.el7.rpm:    
# Utilities for managing cgroups
libmnl: Version 1.0.3-7.el7.rpm:    
# Netlink communication library
libnetfilter_conntrack: Version 1.0.6-1.el7_3.rpm:    
# Library for interacting with Netfilter connection tracking
libnfnetlink: Version 1.0.1-4.el7.rpm:    
# Library for communication with Netfilter subsystem
libseccomp: Version 2.3.1-4.el7.rpm:    
# Library for syscall filtering and sandboxing
libselinux-python: Version 2.5-15.el7.rpm:    
# Python bindings for SELinux
libselinux-utils: Version 2.5-15.el7.rpm:    
# Utilities for managing SELinux policies
libsemanage-python: Version 2.5-14.el7.rpm:    
# Python bindings for SELinux management library
policycoreutils: Version 2.5-34.el7.rpm:    
# Utilities for managing SELinux policies
policycoreutils-python: Version 2.5-34.el7.rpm:    
# Python bindings for SELinux policy management
python-IPy: Version 0.75-6.el7.rpm:    
# Python library for handling IP addresses
selinux-policy: Version 3.13.1-268.el7_9.2.rpm:    
# Core SELinux policy definitions and rules
selinux-policy-targeted: Version 3.13.1-268.el7_9.2.rpm:    
# Targeted SELinux policy configuration
setools-libs: Version 3.3.8-4.el7.rpm:    
# Libraries for interacting with SELinux policy files
slirp4netns: Version 0.4.3-4.el7_8.rpm:    
# User-mode networking stack for containers
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/docker-ce/*.rpm
        - systemctl enable docker
        - systemctl start docker
: verify:
        - systemctl status docker
        - docker version


# KUBERNETES

    - kube/
conntrack-tools-1.4.4-7.el7.x86_64.rpm:  
# Tools for managing the Netfilter connection tracking system
libmnl-1.0.3-7.el7.x86_64.rpm:  
# Minimalistic Netlink communication library
cri-tools-1.29.0-150500.1.1.x86_64.rpm:  
# Container runtime interface tools for Kubernetes
libnetfilter_conntrack-1.0.6-1.el7_3.x86_64.rpm:  
# Library for interacting with Netfilter connection tracking
ebtables-2.0.10-16.el7.x86_64.rpm:  
# Tool for managing Ethernet bridge filtering rules
ethtool-4.8-10.el7.x86_64.rpm:  
# Utility for querying and configuring network interface settings
iproute-4.11.0-30.el7.x86_64.rpm:  
# Advanced IP routing and network device configuration tools
iptables-1.4.21-35.el7.x86_64.rpm:  
# Linux firewall utility for packet filtering
kubeadm-1.29.5-150500.1.1.x86_64.rpm:  
# Tool for Kubernetes cluster management
kubectl-1.29.5-150500.1.1.x86_64.rpm:  
# Command-line interface for interacting with Kubernetes clusters
kubelet-1.29.5-150500.1.1.x86_64.rpm:  
# Kubernetes node agent
kubernetes-cni-1.3.0-150500.1.1.x86_64.rpm:  
# Container Networking Interface plugins for Kubernetes
libnetfilter_cthelper-1.0.0-11.el7.x86_64.rpm:  
# Library for Netfilter connection tracking helpers
libnetfilter_cttimeout-1.0.0-7.el7.x86_64.rpm:  
# Library for Netfilter connection tracking timeouts
libnetfilter_queue-1.0.2-2.el7_2.x86_64.rpm:  
# Library for interacting with Netfilter packet queue
libnfnetlink-1.0.1-4.el7.x86_64.rpm:  
# Library for communication with Netfilter subsystem
socat-1.7.3.2-2.el7.x86_64.rpm:  
# Utility for establishing bidirectional data transfer between two endpoints
tcp_wrappers-libs-7.6-77.el7.x86_64.rpm:  
# Libraries for TCP wrapper functionality
yum-utils-1.1.31-54.el7_8.noarch.rpm:  
# Collection of utilities for managing YUM repositories and packages
: installation: 
        - yum install -y --cacheonly --disablerepo=* /var/rpm_dir/kube/*.rpm
: verify:
        - kubeadm config images list

# #################  KUBERNETES-IMAGES
    - docker-images/
coredns_v1.11.1.tar:
# CoreDNS component for DNS resolution
kube-apiserver_v1.29.5.tar:
# Kubernetes API server for handling, validating, and configuring API objects
kube-proxy_v1.29.5.tar:
# Kubernetes network proxy for routing traffic to pods
pause_3.9.tar:
# Pause container used as a placeholder for pods
etcd_v3.5.12-0.tar:
# Distributed key-value store for storing cluster data
kube-controller-manager_v1.29.5.tar:
#  Kubernetes controller manager for running controller processes
kube-scheduler_v1.29.5.tar:
# kube-scheduler_v1.29.5.tar: Kubernetes scheduler for scheduling pods on nodes
: extraction:     
        - for x in *.tar; do docker load < $x && echo "loaded from file $x"; done;

# #################  NETWORKING
    - networking/
flannel-cni-p:  
# No specific information provided, possible reference to Flannel CNI plugin
flannel-cni-plugin-v1.4.1-flannel1.tar:  
# Archive file containing Flannel CNI plugin version 1.4.1
flannel-v0.25.2.tar:  
# Archive file containing Flannel version 0.25.2
flannel:v0.25.2.tar:  
# Docker image archive for Flannel version 0.25.2
kube-flannel.yml:  
# YAML configuration file defining Kubernetes resources for deploying Flannel
: extraction:     
        - for x in *.tar; do docker load < $x && echo "loaded from file $x"; done;
: installation: 
        - ???

# #################  NGINX
    - nginx/
deploy.yaml:  
# YAML configuration file for deploying Kubernetes resources
kube-webhook-certgen_v1.5.0.tar:  
#  a tool for generating TLS certificates for Kubernetes webhook servers
nginx-controller_v0.41.0.tar:  
#  likely a controller used for managing NGINX instances in a Kubernetes cluster
: extraction:     
        - for x in *.tar; do docker load < $x && echo "loaded from file $x"; done;
: installation: 
        - ???


# ######################### Deploying Kubernetes cluster ########
# Set hostname to 'sdk8s-master'
hostnamectl set-hostname 'sdk8s-master'

# Disable swap
swapoff -a

# Set SELinux to permissive mode
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# Enable required network bridge settings
cat <<EOF > /etc/sysctl.d/k8s.conf
    net.bridge.bridge-nf-call-ip6tables = 1
    net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

# Update /etc/hosts with cluster node IP addresses
# Ensure you replace the IP addresses with the actual IP addresses of your master and worker nodes
<!-- 192.168.22.8    sdk8s-master
192.168.22.40   sdworker-node1
192.168.22.50   sdworker-node2 -->

# Enable kubectl bash completion
echo "source <(kubectl completion bash)" >> ~/.bashrc

# Ensure previous steps are completed on each cluster machine

# Enable kubelet service to start on boot
systemctl enable kubelet.service

# Verify Kubernetes installation
kubectl version

# Initialize Kubernetes cluster with specified pod network CIDR and Kubernetes version
kubeadm init --pod-network-cidr=10.244.0.0/16 --kubernetes-version=v1.29.5 > ~/kubeadm.init.log
# NOTE: The ~/kubeadm.init.log file will contain the token required to join worker nodes to the cluster

# Configure kubectl for current user
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Verify node status
kubectl get nodes

# Add KUBECONFIG environment variable to bashrc, if not already added
grep -q "KUBECONFIG" ~/.bashrc || {
    echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> ~/.bashrc
    . ~/.bashrc
}

# Apply Flannel networking configuration
cd /var/rpm_dir/
kubectl apply -f kube-flannel.yml

# Allow scheduling pods on control plane node for single-node deployments
kubectl taint nodes --all node-role.kubernetes.io/master-

# Join worker nodes to the cluster using kubeadm join command with token and CA certificate hash
kubeadm join 192.168.22.8:6443 --token 49gfys.l4obg3x8flxyc0fr --discovery-token-ca-cert-hash sha256:479203ed42d2884d950ddb81874ea11667788805511b297f3a32c7958fd7fd27

# Verify worker node joining
kubectl get nodes





 

