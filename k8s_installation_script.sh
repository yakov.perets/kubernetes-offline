#!/bin/bash
# installation of kubernetes cluster on  RHEL-7 docker-container-offline as primary for RHEL-VM-7-offline
# on host's Ubuntu-22.04
# i have binary files and docker images on: kubernetes_last_chance/of:
# - yum
# - dm
# - lvm2
# - se
# - docker-ce
# - dbus
# - kube
# and docker images of kubernetes that fits the binarrie's versions.
# - coredns_v1.11.1.tar
# - kube-apiserver_v1.29.5.tar
# - kube-proxy_v1.29.5.tar
# - pause_3.9.tar
# - etcd_v3.5.12-0.tar
# - kube-controller-manager_v1.29.5.tar
# - kube-scheduler_v1.29.5.tar
# and some config file:
# - config.toml
# - containerd.toml
# swapoff -a
# docker run -it --name rhel7-container --privileged centos:7 /bin/bash
# docker cp ~/kubernetes_last_chance/ rhel7-container:/var/rpm_dir/
# docker exec -it rhel7-container /bin/bash
# chmod +x /var/rpm_dir/k8s_installation_script.sh
# ./var/rpm_dir/k8s_installation_script.sh | tee k8s_installation_log.txt
set -e  # Exit immediately if a command exits with a non-zero status
set -x  # Print each command before executing
# Turn off swap
echo "Disabling swap..."
swapoff -a
# set-hostname 'sdk8s-master'
# Add entry to /etc/hosts file
echo "192.168.22.8    sdk8s-master" | tee -a /etc/hosts
# Install necessary RPM packages
echo "Installing RPM packages..."
yum install -y --cacheonly --disablerepo=* /var/rpm_dir/dm/*.rpm
yum install -y --cacheonly --disablerepo=* /var/rpm_dir/lvm2/*.rpm
yum install -y --cacheonly --disablerepo=* /var/rpm_dir/se/*.rpm
yum install -y --cacheonly --disablerepo=* /var/rpm_dir/docker-ce/*.rpm
yum install -y --cacheonly --disablerepo=* /var/rpm_dir/kube/*.rpm
# Disable SELinux enforcement temporarily
# setenforce 0
# Update SELinux configuration to permissive mode
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
# Configure kernel parameters for Kubernetes networking
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
# Apply system-wide kernel parameter changes
sysctl --system || true
# Configure kubectl autocompletion
echo "Configuring kubectl autocompletion..."
echo "source <(kubectl completion bash)" >> ~/.bashrc
# Start Docker
echo "Starting Docker..."
dockerd &
sleep 10
# Verify Docker is running
if ! pgrep -x "dockerd" > /dev/null; then
    echo "Docker failed to start." >&2
    exit 1
fi
echo "Docker started successfully."
# Load Docker images
echo "Loading Docker images..."
cd /var/rpm_dir/docker-images/
for image_tar in *.tar; do
    if [[ -f "$image_tar" ]]; then
        docker load < "$image_tar"
        echo "Loaded Docker image from $image_tar"
    else
        echo "Docker image tar file $image_tar does not exist."
    fi
done
echo "Docker images loaded successfully."
# Copy and extract CNI plugins
echo "Copying and extracting CNI plugins..."
mkdir -p /etc/cni/net.d
cp /var/rpm_dir/networking/flannel-cni-plugin-v1.4.1-flannel1.tar /etc/cni/net.d/
cd /etc/cni/net.d
for plugin_tar in *.tar; do
    if [[ -f "$plugin_tar" ]]; then
        tar -xf "$plugin_tar"
        echo "Extracted CNI plugin from $plugin_tar"
    else
        echo "CNI plugin tar file $plugin_tar does not exist."
    fi
done
echo "CNI plugins extracted successfully."
# Create CNI configuration file
echo "Creating CNI configuration file..."
cat <<EOF > /etc/cni/net.d/10-flannel.conflist
{
  "name": "cni-flannel",
  "cniVersion": "0.3.1",
  "plugins": [
    {
      "type": "flannel",
      "delegate": {
        "isDefaultGateway": true
      }
    }
  ]
}
EOF
echo "CNI configuration file created successfully."
# Start containerd
mkdir -p /var/run/docker/containerd/
mkdir -p /etc/containerd/
cp /var/rpm_dir/config.toml /etc/containerd/
cp /var/rpm_dir/containerd.toml /var/run/docker/containerd/
echo "Starting containerd..."
containerd -c /etc/containerd/config.toml &
sleep 10
# Verify containerd is running
if ! pgrep -x "containerd" > /dev/null; then
    echo "containerd failed to start." >&2
    exit 1
fi
echo "containerd started successfully."
# restart
pkill dockerd
sleep 10
pkill containerd
sleep 10
dockerd &
sleep 10
containerd -c /etc/containerd/config.toml &
sleep 10
# Generate kubeadm config to point kubeadm to grab images from local docker
echo "Generating kubeadm config..."
cat <<EOF > /etc/kubernetes/kubeadm-config.yaml
apiVersion: kubeadm.k8s.io/v1beta3
kind: ClusterConfiguration
imageRepository: "my-registry"
kubernetesVersion: "v1.29.5"
networking:
  podSubnet: "10.244.0.0/16"
EOF
echo "kubeadm config generated."
# Start kubelet manually
echo "Starting kubelet..."
ln -s /usr/lib/systemd/system/kubelet.service /etc/systemd/system/multi-user.target.wants/kubelet.service
ls -l /etc/systemd/system/multi-user.target.wants/kubelet.service
systemctl is-enabled kubelet

# kubelet &
sleep 10
# Verify kubelet is running
# if ! pgrep -x "kubelet" > /dev/null; then
#     echo "kubelet failed to start." >&2
#     exit 1
# fi
echo "kubelet started successfully."
# Initialize Kubernetes cluster
echo "Initializing Kubernetes cluster..."
kubeadm init --config /etc/kubernetes/kubeadm-config.yaml --ignore-preflight-errors=ImagePull
# Set up kubectl for the root user
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# Create kubelet service override configuration
sudo mkdir -p /etc/systemd/system/kubelet.service.d/
sudo tee /etc/systemd/system/kubelet.service.d/10-kubeadm.conf <<EOF
[Service]
EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env
Environment="KUBELET_EXTRA_ARGS=--container-runtime=remote --container-runtime-endpoint=unix:///run/containerd/containerd.sock"
EOF

# Edit kubeadm flags environment file
sudo tee -a /var/lib/kubelet/kubeadm-flags.env <<EOF
KUBELET_KUBEADM_ARGS=--container-runtime=remote --container-runtime-endpoint=unix:///run/containerd/containerd.sock
EOF
# Create .crictl.yaml configuration file
cat <<EOF > ~/.crictl.yaml
runtime-endpoint: unix:///run/containerd/containerd.sock
image-endpoint: unix:///run/containerd/containerd.sock
EOF

# Check crictl pods and containers
crictl --runtime-endpoint unix:///run/containerd/containerd.sock ps -a
crictl --runtime-endpoint unix:///run/containerd/containerd.sock pods
# Check the status of Kubernetes components
echo "Checking Kubernetes component statuses..."
kubectl get nodes
# Configure kubectl to manage the cluster
grep -q "KUBECONFIG" ~/.bashrc || {
    echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> ~/.bashrc
    . ~/.bashrc
}
kubectl apply -f /var/rpm_dir/networking/kube-flannel.yml
kubectl taint nodes --all node-role.kubernetes.io/master-
echo "Installation and configuration completed successfully."

 
# stop
# docker stop rhel7-container
# docker rm rhel7-container

thanks!